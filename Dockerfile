ARG SOURCE_DOCKER_REGISTRY=localhost:5000
ARG BASE_DOCKER_TAG=3.11.6

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_opt_autodockvina:1.1.2 AS opt_autodockvina

FROM ${SOURCE_DOCKER_REGISTRY}/alpine_base:${BASE_DOCKER_TAG}

RUN apk update && \
    apk add bash

COPY --from=build /opt/ /opt/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/


